## About me

My name is Suraj. I am based out of Mumbai, India. My usual working hours are
10:30 am to 6:00 pm IST (5am to 12:30pm UTC).

### Contacting me regarding work
I tend to work mostly of my Todo list, if you want my attention please tag me on
any issue using my handle and I will try to get to it within 48hrs.

I don't mind Sync calls to discuss some topics and close things off quickly
instead ping-pong back and forth of comments, but I would prefer to get some
intimation before the call is setup. Ideally 24 hours before, so that I can
prioritize my work accordingly. I don't personally encourage it, but I
understand and sometimes admire the speed at which things get done when people
talk face to face.

### Informal Meetings
I love to have a coffee-chat, please feel free to find a time in my
calander that works for the both of us. I am usually fine if you block my time
until 6:30 pm IST, but I would prefer if its between my working hours. Feel free
to ping me on slack if the timing isn't working out, we can try and find a
common ground in most of the scenario!

For people who are visiting Mumbai, I am even open to working together as a form
of Get Togther. Please ping me on Slack and we can try and decide on the
details.

### Feedback and Improvements
If you wish to give me a feedback please use this Anonymous Google form:
https://forms.gle/MqZzo34WgjjVuERM8

### Contact Details

- Official Email: stripathi@gitlab.com
- Personal Email: tripathy.suraj0704@gmail.com
- Website: surajtripathi.info
